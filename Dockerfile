FROM openjdk:8-jdk-alpine
ADD build/libs/* app.jar
ENTRYPOINT ["java","-jar","/app.jar"]